import Foundation

struct ElectricityTariff: Comparable, CustomStringConvertible {
  let id: String
  var denomination: String
  var provider: String
  var usageProfile: UsageProfile
  var powerUnit: String {
    switch self.usageProfile {
    case .household:
      return PowerType.active.rawValue
    case .industrial:
      return PowerType.apparent.rawValue
    }
  }
  var currencyCode: String
  var currencySymbol: String {
    return self.getSymbolForCurrencyCode(code: currencyCode)!
  }
  var fixedRate: Decimal?
  var consumptionPrice: Decimal?
  var productionRevenue: Decimal?
  
  // Declaring description property to conform CustomStringConvertible
  var description: String {
    return ( self.provider + " - " + (!self.denomination.isBlank() ? self.denomination : self.id))
  }

  init(label: String = String(), provider: String = String(), usage: UsageProfile = UsageProfile.household, currency: String = "EUR") {
    self.id = UUID().uuidString
    self.denomination = label
    self.provider = provider
    self.usageProfile = usage
    self.currencyCode = Locale.isoCurrencyCodes.contains(currency) ? currency : "EUR"
  }
  
  func getProductionAmount(powerValue: PowerValue?, timestamp: Date?) -> Amount? {
    guard let power = powerValue, let time = timestamp else { return nil }
//    let hour = Calendar.current.component(.hour, from: time)
    guard let price = productionRevenue else { return nil }
    let amount = Amount(value: Decimal(power.value) * price, currencySymbol: self.currencySymbol)
    return amount
  }
  
  func getConsumptionAmount(powerValue: PowerValue?, timestamp: Date?) -> Amount? {
    guard let power = powerValue, let time = timestamp else { return nil }
//    let hour = Calendar.current.component(.hour, from: time)
    guard let price = consumptionPrice else { return nil }
    let amount = Amount(value: Decimal(power.value) * price, currencySymbol: self.currencySymbol)
    return amount
  }
  
  func getConnectionCostForHour(timestamp: Date?) -> Amount? {
    guard let time = timestamp, let cost = self.fixedRate else { return nil }
    let calendar = Calendar.current
    let startOfMonth = time.startOfMonth
    let endOfMonth = time.endOfMonth
    let diffComponents = calendar.dateComponents([.hour], from: startOfMonth, to: endOfMonth)
    let hours = diffComponents.hour!
    let value: Decimal = -(cost/Decimal(integerLiteral: hours))
    let amount = Amount(value: value, currencySymbol: self.currencySymbol)
    return amount
  }
  
  private func getSymbolForCurrencyCode(code: String) -> String? {
    let result = Locale.availableIdentifiers.map { Locale(identifier: $0) }.first { $0.currencyCode == code }
    return result?.currencySymbol
  }
}

// Implementing the == and the < operators to conform Comparable
extension ElectricityTariff {
  static func ==(lhs: Self, rhs: Self) -> Bool {
    return lhs.id == rhs.id
  }
  
  static func <(lhs: Self, rhs: Self) -> Bool {
    if !lhs.denomination.isBlank() && !rhs.denomination.isBlank() {
      // both have a name, compare their names
      return lhs.denomination < rhs.denomination
    } else if !lhs.denomination.isBlank() {
      // first has a name
      return true
    } else if !rhs.denomination.isBlank() {
      // second has a name
      return false
    } else {
      // none of them has a name, compare their ids
      return lhs.id < rhs.id
    }
  }
}

struct Amount {
  var value: Decimal
  let currencySymbol: String
}

extension Amount {
  static func + (lhs: Amount, rhs: Amount) -> Amount {
    if lhs.currencySymbol == rhs.currencySymbol {
      return Amount(value: lhs.value + rhs.value, currencySymbol: lhs.currencySymbol)
    } else {
      return Amount(value: 0, currencySymbol: "none")
    }
  }
}

enum PaymentFrequency: Int {
  case monthly = 1
  case quaterly = 3
  case semiannual = 6
  case yearly = 12
}
