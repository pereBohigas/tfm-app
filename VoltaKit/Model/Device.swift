import CoreLocation

struct Device: VoltaElement, Codable {
  let id: String
  var label: String?
  
  var manufacturer: String?
  var model: String?
  var serialNumber: String?
  
  var location: Location?
  var locationDescription: String?
  
  var identificationMethod: IdentificationMethod?
  var connectionType: ConnectionType?
  var loadType: LoadType?
  var energyEfficiency: EnergyEfficiencyClass?
  var lastTimeActive: Date?
  
  var currentPower = [CurrentPowerDevice]()
  var accumulatedPower = [AccumulatedEnergy]()
  var maximumValues = [MaximumValues]()
  
  var description: String {
    return ("Device - " + (self.label ?? self.id))
  }
}

// Declaring CodingKeys enum to conform Codable
extension Device {
  private enum CodingKeys: String, CodingKey {
    case id = "deviceId"
    case label
    case location
    case connectionType
    case locationDescription
    case loadType
    case energyEfficiency
    case identificationMethod
    case manufacturer
    case model
    case serialNumber
  }
}

struct CurrentPowerDevice: CurrentData, PowerData, Codable {
  // Protocol attributtes
  let measurementTimestamp: Date
  let activePower: PowerValue
  // reactivePower value computed on init
  let reactivePower: PowerValue
  let apparentPower: PowerValue
  
  init(timestamp: Date, activeValue: Double, apparentValue: Double) {
    self.measurementTimestamp = timestamp
    self.activePower = PowerValue(value: activeValue, type: .active)
    self.apparentPower = PowerValue(value: apparentValue, type: .apparent)
    
    // Computed properties
    let reactivePowerValue = sqrt(pow(apparentValue, 2) - pow(activeValue, 2))
    self.reactivePower = PowerValue(value: reactivePowerValue,type: .reactive)
  }
}

extension CurrentPowerDevice {
  enum CodingKeys: String, CodingKey {
    case measurementTimestamp
    case activePower
    case apparentPower
  }
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    let measurementTimestamp = try values.decode(Date.self, forKey: .measurementTimestamp)
    let activePowerValue = try values.decode(Double.self, forKey: .activePower)
    let apparentPowerValue = try values.decode(Double.self, forKey: .apparentPower)
    
    self.init(timestamp: measurementTimestamp, activeValue: activePowerValue, apparentValue: apparentPowerValue)
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    
    try container.encode(measurementTimestamp, forKey: .measurementTimestamp)
    try container.encode(activePower.value, forKey: .activePower)
    try container.encode(apparentPower.value, forKey: .apparentPower)
  }
}

enum IdentificationMethod: String, Codable {
  case automatic
  case manual
}

enum LoadType: String, Codable {
  case resistive
  case inductive
  case capacitive
  case resistiveInductive = "resistive-inductive"
  case resistiveCapacitive = "resistive-capacitive"
}

enum EnergyEfficiencyClass: String, Codable {
  case Aplusplusplus = "A+++"
  case Aplusplus = "A++"
  case Aplus = "A+"
  case A
  case B
  case C
  case D
  case E
  case F
  case G
}
