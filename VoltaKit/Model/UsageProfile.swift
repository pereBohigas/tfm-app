enum UsageProfile: String, CaseIterable {
  case household
  case industrial
}
