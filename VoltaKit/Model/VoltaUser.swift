import Foundation

struct VoltaUser {
  let username: String
  
  var locale: Locale
  let costOrRevenueFormatter: NumberFormatter = NumberFormatter()
  let powerValuesFormatter: NumberFormatter = NumberFormatter()
  let percentageFormatter: NumberFormatter = NumberFormatter()
  
  var boxes: [String: Box]
  var tariffs: [String: ElectricityTariff]
  
  init(username: String, localeIdentifier: String) {
    self.username = username
    self.locale = Locale(identifier: localeIdentifier)
    
    self.boxes = [String: Box]()
    self.tariffs = [String: ElectricityTariff]()
    
    self.setFormatters()
  }
  
  private func setFormatters() {
    self.powerValuesFormatter.locale = self.locale
    self.powerValuesFormatter.numberStyle = .decimal
    self.powerValuesFormatter.roundingMode = .halfUp
    self.powerValuesFormatter.groupingSize = 3
    self.powerValuesFormatter.maximumFractionDigits = 3
    self.powerValuesFormatter.minimumFractionDigits = 3
    self.powerValuesFormatter.decimalSeparator = ","
    
    self.costOrRevenueFormatter.locale = self.locale
    //        self.costOrRevenueFormatter.numberStyle = .currency
    self.costOrRevenueFormatter.positivePrefix = self.costOrRevenueFormatter.plusSign + " "
    self.costOrRevenueFormatter.negativePrefix = self.costOrRevenueFormatter.minusSign + " "
    self.costOrRevenueFormatter.roundingMode = .halfUp
    self.costOrRevenueFormatter.groupingSize = 3
    self.costOrRevenueFormatter.maximumFractionDigits = 2
    self.costOrRevenueFormatter.minimumFractionDigits = 2
    
    self.percentageFormatter.locale = self.locale
    self.percentageFormatter.roundingMode = .halfUp
    self.percentageFormatter.maximumFractionDigits = 2
    self.percentageFormatter.minimumFractionDigits = 2
  }
  
  func getTariff(boxId: String) -> ElectricityTariff? {
    if let tariffId = boxes[boxId]?.associatedTariffId, let tariff = tariffs[tariffId] {
      return tariff
    } else {
      return nil
    }
  }
  
  func getUsage(boxId: String) -> UsageProfile {
    if let tariff = self.getTariff(boxId: boxId) {
      return tariff.usageProfile
    } else {
      // box without associated tariff, default usage household
      return .household
    }
  }
  
  func getLastPowerValue(boxId: String) -> (power: PowerValue, timestamp: Date)? {
    return boxes[boxId]?.getLastCurrentPowerValue(usage: self.getUsage(boxId: boxId))
  }
  
  func getLastPowerValue(boxId: String, deviceId: String) -> (power: PowerValue, timestamp: Date)? {
    return boxes[boxId]?.getLastCurrentPowerValue(usage: self.getUsage(boxId: boxId), deviceId: deviceId)
  }
  
  func formatEnergyValue(_ powerValue: PowerValue?) -> (value: String, unit: String) {
    let valueString: String
    let unitString: String
    if powerValue != nil, powerValue?.value != 0 {
      valueString = self.powerValuesFormatter.string(from: NSNumber(value: abs(powerValue!.value))) ?? "—"
      unitString = " " + powerValue!.unit.rawValue + "h"
    } else {
      valueString = "—"
      unitString = ""
    }
    return (valueString, unitString)
  }
  
  func formatAmount(_ amount: Amount?) -> (value: String, unit: String) {
    let valueString: String
    if amount != nil, amount?.value != 0 {
      valueString = self.costOrRevenueFormatter.string(for: NSDecimalNumber(decimal: amount!.value)) ?? "0,00"
    } else {
      valueString = "0,00"
    }
    let unitString = " " + self.costOrRevenueFormatter.currencySymbol + "/hour"
    return (valueString, unitString)
  }
  
  func formatPercentage(_ percentage: Double) -> String {
    return (self.percentageFormatter.string(for: abs(percentage)) ?? String()) + " %"
  }
}
