import XCTest
@testable import VoltaMockServer

class VoltaTests: XCTestCase {
  var sut: VoltaUser!
  
  override func setUp() {
    super.setUp()
    sut = VoltaUser(username: "testUser", boxes: [String: Box]())
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    sut = nil
    super.tearDown()
    
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }
  
  //    func testExample() throws {
  //        // This is an example of a functional test case.
  //        // Use XCTAssert and related functions to verify your tests produce the correct results.
  //    }
  //
  //    func testPerformanceExample() throws {
  //        // This is an example of a performance test case.
  //        self.measure {
  //            // Put the code you want to measure the time of here.
  //        }
  //    }
  
}
