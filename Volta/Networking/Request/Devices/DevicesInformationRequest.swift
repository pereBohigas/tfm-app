struct DevicesInformationRequest: ApiRequest {
  let method = RequestType.GET
  var pathVariable = "/information/boxes/"
  var queryParameters: [String : String]? = nil
  var bodyParameters: [String : String]? = nil
  
  init(boxId: String) {
    pathVariable.append(boxId + "/devices")
  }
  
  init(boxId: String, deviceId: String) {
    pathVariable.append(boxId + "/devices/" + deviceId)
  }
}
