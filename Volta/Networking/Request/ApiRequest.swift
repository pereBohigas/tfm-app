import Foundation

protocol ApiRequest {
  var method: RequestType { get }
  var pathVariable: String { get set }
  var queryParameters: [String : String]? { get set }
  var bodyParameters: [String : String]? { get set }
}

extension ApiRequest {
  func request(with baseURL: URL) -> URLRequest {
    guard var components = URLComponents(url: baseURL.appendingPathComponent(pathVariable), resolvingAgainstBaseURL: false) else {
      fatalError("Unable to create URL components")
    }
    
    components.queryItems = queryParameters?.map {
      URLQueryItem(name: String($0), value: String($1))
    }
    
    guard let url = components.url else {
      fatalError("Unable to retrieve URL")
    }
    
    var request = URLRequest(url: url)
    request.httpMethod = method.rawValue
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    if let body = bodyParameters {
      do {
        request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
      } catch let error {
        print(error.localizedDescription)
      }
    }
    
    return request
  }
}

public enum RequestType: String {
  case GET, POST, PUT
}
