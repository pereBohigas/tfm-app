struct BoxesInformationRequest: ApiRequest {
  let method = RequestType.GET
  var pathVariable = "/information/boxes"
  var queryParameters: [String : String]? = nil
  var bodyParameters: [String : String]? = nil
  
  init() {
  }
  
  init(boxId: String){
    pathVariable.append("/" + boxId)
  }
}
