struct BoxesCurrentPowerRequest: ApiRequest {
  let method = RequestType.GET
  var pathVariable = "/current_power/boxes"
  var queryParameters: [String : String]? = nil
  var bodyParameters: [String : String]? = nil
  
  init(){
  }
  
  init(boxId: String){
    pathVariable.append("/" + boxId)
  }
}
