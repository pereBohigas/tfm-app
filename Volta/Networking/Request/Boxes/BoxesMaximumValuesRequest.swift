struct BoxesMaximumValuesRequest: ApiRequest {
  let method = RequestType.GET
  var pathVariable = "/maximum_values/boxes"
  var queryParameters: [String : String]? = [String : String]()
  var bodyParameters: [String : String]? = nil
  
  init(from fromTimestamp: String, to toTimestamp: String) {
    queryParameters?["from"] = fromTimestamp
    queryParameters?["to"] = toTimestamp
  }
  
  init(boxId: String, from fromTimestamp: String, to toTimestamp: String) {
    pathVariable.append("/" + boxId)
    queryParameters?["from"] = fromTimestamp
    queryParameters?["to"] = toTimestamp
  }
}

