import Foundation

class Session {
  var user: VoltaUser
  let restClient: ApiClient
  
  var token: String
  
  init(user: VoltaUser, restClient: ApiClient, token: String) {
    self.user = user
    self.restClient = restClient
    self.token = token
  }
}
