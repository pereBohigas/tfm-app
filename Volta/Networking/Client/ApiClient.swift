import Foundation
import RxSwift

protocol ApiClient {
  var baseURL: URL { get }
  
  func request<T: ApiResponse>(with apiRequest: ApiRequest) -> Single<(Int, T)>
}
