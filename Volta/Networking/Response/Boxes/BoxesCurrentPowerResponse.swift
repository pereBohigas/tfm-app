import Foundation

struct BoxesCurrentPowerResponse: ApiResponse {
  let responseTimestamp: Date
  let data: [SingleBoxCurrentPowerResponse]
}

struct SingleBoxCurrentPowerResponse: Codable {
  let boxId: String
  let currentPower: CurrentPowerBox
  let lastTimeActive: Date?
}
