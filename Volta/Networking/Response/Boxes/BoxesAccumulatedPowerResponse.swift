import Foundation

struct BoxesAccumulatedEnergyResponse: ApiResponse {
  let responseTimestamp: Date
  let data: [SingleBoxAccumulatedEnergyResponse]
}

struct SingleBoxAccumulatedEnergyResponse: Codable {
  let boxId: String
  let maximumValues: AccumulatedEnergy
}
