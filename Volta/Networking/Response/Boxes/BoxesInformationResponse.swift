import Foundation

struct BoxesInformationResponse: ApiResponse {
  let responseTimestamp: Date
  let data: [Box]
}
