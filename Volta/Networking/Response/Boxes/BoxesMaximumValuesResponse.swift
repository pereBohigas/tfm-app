import Foundation

struct BoxesMaximumValuesResponse: ApiResponse {
  let responseTimestamp: Date
  let data: [SingleBoxMaximumValuesResponse]
}

struct SingleBoxMaximumValuesResponse: Codable {
  let boxId: String
  let maximumValues: MaximumValues
}
