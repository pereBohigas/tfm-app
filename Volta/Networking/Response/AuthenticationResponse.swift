import Foundation

struct AuthenticationResponse: ApiResponse {  
  let responseTimestamp: Date
  let data: AuthenticationResponseData
  
  struct AuthenticationResponseData: Codable {
    let userName: String
    let token: String?
    let message: String?
  }
}
