import Foundation

struct DevicesAccumulatedEnergyResponse: ApiResponse {
  let responseTimestamp: Date
  let data: [SingleDeviceAccumulatedEnergyResponse]
}

struct SingleDeviceAccumulatedEnergyResponse: Codable {
  let deviceId: String
  let maximumValues: AccumulatedEnergy
}
