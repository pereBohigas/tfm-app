import Foundation

struct DevicesInformationResponse: ApiResponse {
  let responseTimestamp: Date
  let data: [Device]
}
