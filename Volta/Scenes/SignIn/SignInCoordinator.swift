import RxSwift

class SignInCoordinator: BaseCoordinator {
  private let disposeBag = DisposeBag()
  private let viewModel: SignInViewModel
  
  init(client: ApiClient) {
    self.viewModel = SignInViewModel(apiClient: client)
  }
  
  override func start() {
    let viewController = SignInViewController.instantiate()
    viewController.viewModel = self.viewModel

    self.navigationController.viewControllers = [viewController]
    self.setUpBindings()
  }
  
  func didFailSignIn(errorDescription: String) {
    self.navigationController.showAlert(title: "Error", message: errorDescription)
  }
  
  private func setUpBindings() {
    // Coordinator subscribes to didSingIn event and notifies parentCoordinator
    viewModel.didSignIn
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { [weak self] newSession in
        guard let signInCoordinator = self else { return }
        signInCoordinator.navigationController.viewControllers = []
        signInCoordinator.parentCoordinator?.release(coordinator: signInCoordinator)
        (signInCoordinator.parentCoordinator as? SignInListener)?.didSignIn(session: newSession)
      })
      .disposed(by: self.disposeBag)
    
    viewModel.didFailSignIn
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { [weak self] error in
        guard self != nil else { return }
        self?.didFailSignIn(errorDescription: error)
      })
      .disposed(by: self.disposeBag)
  }
}
