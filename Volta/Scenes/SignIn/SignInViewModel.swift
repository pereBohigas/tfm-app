import RxSwift
import RxCocoa

class SignInViewModel {
  private let disposeBag = DisposeBag()
  private var apiClient: ApiClient
  
  // Credentials
  let userName = BehaviorSubject<String>(value: String())
  let password = BehaviorSubject<String>(value: String())
  
  // UI
  var isSignInActive = BehaviorRelay<Bool>(value: false)
  let isLoading = BehaviorRelay<Bool>(value: false)
  
  // Events
  let didSignIn = PublishSubject<Session>()
  let didFailSignIn = PublishSubject<String>()
  
  init(apiClient: ApiClient) {
    self.apiClient = apiClient
    self.setUpBindings()
  }
  
  func signIn() {
    self.isLoading.accept(true)
    
    Observable
      .combineLatest(self.userName, self.password, self.isSignInActive)
      .take(1)
      .filter { _, _, active in active }
      .map { userName, password, _ in
        return AuthenticationRequest(userName: userName, password: password)
      }
      .flatMapLatest { [weak self] request -> Single<(Int, AuthenticationResponse)> in
        return (self?.apiClient.request(with: request))!
      }
      .subscribe(
        onNext: { [weak self] status, response in
          switch (String(status).first) {
          case "2": // Success
            print("Login in system successful")
            // Hardcoded catalan locale for the default user
            let loggedUser: VoltaUser = VoltaUser(username: response.data.userName, localeIdentifier: "ca_ES")
            self?.didSignIn.onNext(Session(user: loggedUser, restClient: (self?.apiClient)!, token: (response.data.token ?? String())))
          default: // Other Response status codes (including 4xx client error)
            print("Login in system failed with status code \(status) and message '\(response.data.message ?? "")'")
            self?.didFailSignIn.onNext(response.data.message ?? String())
          }
          self?.isLoading.accept(false)
        }, onError: { [weak self] error in
          self?.didFailSignIn.onNext("An error ocurred while making request")
          print("Error: \(error)")
          self?.isLoading.accept(false)
        }
      )
      .disposed(by: self.disposeBag)
  }
  
  private func setUpBindings() {
    Observable
      .combineLatest(self.userName, self.password)
      .map { !$0.isEmpty && !$1.isEmpty }
      .bind(to: self.isSignInActive)
      .disposed(by: self.disposeBag)
  }
  
}
