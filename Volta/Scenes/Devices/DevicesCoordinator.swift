import RxSwift
import RxRelay

class DevicesCoordinator: BaseCoordinator {
  private let disposeBag = DisposeBag()
  private let viewModel: DevicesViewModel
  
  init?(session: Session, boxId: String) {
    guard let devicesViewModel = DevicesViewModel(session: session, boxId: boxId) else { return nil }
    self.viewModel = devicesViewModel
  }
  
  override func start() {
    let viewController = DevicesViewController.instantiate()
    viewController.viewModel = self.viewModel
    
    self.navigationController.pushViewController(viewController, animated: true)
    self.setUpBindings()
  }
  
  func presentDetailsScene(session: Session, boxId: String, deviceId: String) {
    guard let detailsCoordinator = DetailsCoordinator(session: session, boxId: boxId, deviceId: deviceId) else { return }
    detailsCoordinator.navigationController = self.navigationController
    self.coordinate(to: detailsCoordinator)
  }
  
  private func setUpBindings() {
    self.viewModel.isDeviceInfoSelected
      .observeOn(MainScheduler.instance)
      .bind { [unowned self] session, boxId, deviceId in
        self.presentDetailsScene(session: session, boxId: boxId, deviceId: deviceId)
      }
      .disposed(by: disposeBag)
  }
}
