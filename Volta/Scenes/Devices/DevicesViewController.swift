import UIKit
import RxSwift
import RxCocoa

class DevicesViewController: UIViewController, Storyboarded {
  static var storyboard = AppStoryboard.devices
  private let disposeBag = DisposeBag()
  private var powerUnitsOrPercentage = true
  
  var viewModel: DevicesViewModel?
  
  var percentageButton: UIBarButtonItem?
  
  // UI
  @IBOutlet weak var tableView: UITableView!
  
  @IBOutlet weak var devicesActiveLabel: UILabel! {
    didSet {
      devicesActiveLabel.font = devicesActiveLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var devicesAttachedLabel: UILabel! {
    didSet {
      devicesAttachedLabel.font = devicesAttachedLabel.font.monospacedDigitFont
    }
  }
  
  @IBOutlet weak var productionPowerValueLabel: UILabel! {
    didSet {
      productionPowerValueLabel.font = productionPowerValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var productionPowerUnitLabel: UILabel!
  @IBOutlet weak var productionRevenueValueLabel: UILabel! {
    didSet {
      productionRevenueValueLabel.font = productionRevenueValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var productionRevenueUnitLabel: UILabel!
  
  @IBOutlet weak var consumptionPowerValueLabel: UILabel! {
    didSet {
      consumptionPowerValueLabel.font = consumptionPowerValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var consumptionPowerUnitLabel: UILabel!
  @IBOutlet weak var consumptionCostValueLabel: UILabel! {
    didSet {
      consumptionCostValueLabel.font = consumptionCostValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var consumptionCostUnitLabel: UILabel!
  
  override func viewDidLoad() {
    title = "\(viewModel?.selectedBox.label ?? viewModel?.selectedBox.id ?? "")"
    //        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "percent", style: .done, target: self, action: #selector(showPercentagesOrValue))
    self.tableView.contentInset.bottom = 87
    
    percentageButton = UIBarButtonItem(title: "%", style: .plain, target: self, action: nil)
    
    self.navigationItem.rightBarButtonItem = percentageButton
    
    self.setUpBindings()
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    viewModel?.isPolling.accept(true)
    super .viewWillAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    viewModel?.isPolling.accept(false)
    super .viewWillDisappear(animated)
  }
  
  private func setUpBindings() {
    guard let viewModel = self.viewModel, let tableView = self.tableView else { return }
    
    viewModel.tableDataSource
      .bind(to: tableView.rx.items(cellIdentifier: "DeviceCell")) { row, device, cell in
        if let deviceCell = cell as? DeviceCell {
          let user = viewModel.activeSession.user
          
          // Label
          deviceCell.deviceIdentificationLabel.text = device.label ?? device.id
          
          // Power activity
          let currentPower = user.getLastPowerValue(boxId: viewModel.selectedBox.id, deviceId: device.id)
          if self.powerUnitsOrPercentage {
            let currentPowerString = user.formatEnergyValue(currentPower?.power)
            deviceCell.powerActivityOrInactivityTimeLabel.text = currentPowerString.value + currentPowerString.unit
          } else {
            if currentPower != nil, currentPower!.power.value > 0 {
              let percentage = currentPower!.power.value / viewModel.displayedTotalValues.consumption.power.value
              deviceCell.powerActivityOrInactivityTimeLabel.text = user.formatPercentage(percentage)
            } else if currentPower != nil, currentPower!.power.value < 0 {
              let percentage = currentPower!.power.value / viewModel.displayedTotalValues.production.power.value
              deviceCell.powerActivityOrInactivityTimeLabel.text = user.formatPercentage(percentage)
            }
          }
          
          // Cell apperance
          if currentPower == nil || currentPower!.power.value == 0 {
            // Device off
            if let lastActiveDate: Date = device.lastTimeActive {
              let timeIntervalString = Date().timeIntervalSince(lastActiveDate).stringFromTimeInterval()
              deviceCell.powerActivityOrInactivityTimeLabel.text = "active \(timeIntervalString) ago"
            } else {
              deviceCell.powerActivityOrInactivityTimeLabel.text = "inactivity time unavailable"
            }
            deviceCell.powerActivityOrInactivityTimeLabel.font = deviceCell.powerActivityOrInactivityTimeLabel.font.withSize(12)
          } else {
            // Active devices
            if currentPower!.power.value > 0 {
              // Device producing energy
              deviceCell.cellBackground.backgroundColor = AppColors.mainLightGreen
            } else {
              // Device consuming energy
              deviceCell.cellBackground.backgroundColor = AppColors.consumingBlue
            }
          }
        }
        cell.selectionStyle = .none
    }
    .disposed(by: self.disposeBag)
    
    viewModel.totalsDataSource
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { [weak self] newTotals in
        let user = viewModel.activeSession.user
        
        self?.devicesActiveLabel.text = String(newTotals.devicesUsage.active) + " devices active of "
        self?.devicesAttachedLabel.text = " " + String(newTotals.devicesUsage.attached) + " devices attached"
        
        let productionPower = user.formatEnergyValue(newTotals.production.power)
        self?.productionPowerValueLabel.text = productionPower.value
        self?.productionPowerUnitLabel.text = productionPower.unit
        
        let productionRevenue = user.formatAmount(newTotals.production.revenue)
        self?.productionRevenueValueLabel.text = productionRevenue.value
        self?.productionRevenueUnitLabel.text = productionRevenue.unit
        
        let consumptionPower = user.formatEnergyValue(newTotals.consumption.power)
        self?.consumptionPowerValueLabel.text = consumptionPower.value
        self?.consumptionPowerUnitLabel.text = consumptionPower.unit
        
        let consumptionCost = user.formatAmount(newTotals.consumption.cost)
        self?.consumptionCostValueLabel.text = consumptionCost.value
        self?.consumptionCostUnitLabel.text = consumptionCost.unit
      })
      .disposed(by: self.disposeBag)
    
    tableView.rx.itemSelected
      .subscribe(onNext: { indexPath in
        viewModel.selectedDeviceForInfo(indexPath.row)
      })
      .disposed(by: self.disposeBag)
    
    viewModel.isShowingPercentages
      .subscribe(onNext: { [unowned self] status in
        if status {
          self.percentageButton?.title = "%"
        } else {
          if let powerUnit = viewModel.activeSession.user.getTariff(boxId: viewModel.selectedBox.id)?.powerUnit {
            self.percentageButton?.title = powerUnit + "h"
          }
        }
      })
    
    self.percentageButton?.rx.tap
      .bind { [unowned self] _ in
        if self.powerUnitsOrPercentage {
          self.powerUnitsOrPercentage = false
          viewModel.isShowingPercentages.accept(false)
        } else {
          self.powerUnitsOrPercentage = true
          viewModel.isShowingPercentages.accept(true)
        }
      }
      .disposed(by: self.disposeBag)
  }
  
  @objc public func showPercentagesOrValue() {
    
  }
}
