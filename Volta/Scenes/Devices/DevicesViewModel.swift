import RxSwift
import RxCocoa
import Foundation

class DevicesViewModel {
  let disposeBag = DisposeBag()
  var activeSession: Session
  var selectedBox: (id: String, label: String?)
  
  // UI
  var displayedDevices: [Device]
  var displayedTotalValues: DevicesTotalValues
  let isShowingPercentages = BehaviorRelay<Bool>(value: false)
  let tableDataSource = BehaviorSubject(value: [Device]())
  let totalsDataSource = BehaviorSubject(value: DevicesTotalValues())
  
  // Events
  let isPolling = BehaviorRelay<Bool>(value: false)
  let isDeviceInfoSelected = PublishSubject<(Session, boxId: String, deviceId: String)>()
  
  init?(session: Session, boxId: String) {
    self.activeSession = session
    guard let box = activeSession.user.boxes[boxId] else { return nil }
    self.selectedBox = (box.id, box.label)
    
    self.displayedDevices = [Device]()
    self.displayedTotalValues = DevicesTotalValues()
    
    self.setUpBindings()
    self.fetchDevicesCurrentPowerData()
  }
  
  private func setUpBindings() {
    self.isPolling
      .flatMapLatest {  isPolling in
        isPolling ? Observable<Int>.interval(.seconds(1), scheduler: MainScheduler.instance) : .empty()
      }
      .bind { _ in self.fetchDevicesCurrentPowerData() }
      .disposed(by: self.disposeBag)
  }
  
  func fetchDevicesCurrentPowerData() {
    Observable
      .just(DevicesCurrentPowerRequest(boxId: self.selectedBox.id))
      .flatMapLatest { [weak self] request -> Single<(Int, DevicesCurrentPowerResponse)> in
        (self?.activeSession.restClient.request(with: request))!
      }
      .subscribe(
        onNext: { [weak self] _, response in
          self?.loadDevicesReceivedPowerData(receivedPowerData: response.data)
        }, onError: { error in
          print("An error ocurred while a request was being made: \(error)")
        }
      )
      .disposed(by: self.disposeBag)
  }
  
  func loadDevicesReceivedPowerData(receivedPowerData: [SingleDeviceCurrentPowerResponse]) {
    var needsUpdate: Bool = false
    _ = receivedPowerData.map { (newPowerData: SingleDeviceCurrentPowerResponse) -> () in
      if let existingDevice = self.activeSession.user.boxes[self.selectedBox.id]?.devices[newPowerData.deviceId] {
        if !existingDevice.currentPower.contains(newPowerData.currentPower) {
          self.activeSession.user.boxes[selectedBox.id]!.devices[newPowerData.deviceId]!.lastTimeActive = newPowerData.lastTimeActive
          self.activeSession.user.boxes[selectedBox.id]!.devices[newPowerData.deviceId]!.currentPower.append(newPowerData.currentPower)
          needsUpdate = true
        }
      }
    }
    if needsUpdate {
      guard let devicesToUpdate = self.activeSession.user.boxes[self.selectedBox.id]?.devices else { return }
      self.updateTableData(devicesNewStatus: devicesToUpdate.values.map { $0 })
      self.updateTotalValues()
    }
  }
  
  func updateTotalValues() {
    self.displayedTotalValues = DevicesTotalValues()
    guard let hostBox = self.activeSession.user.boxes[self.selectedBox.id] else { return }
    
    var tariff: ElectricityTariff? = nil
    if let tariffId = hostBox.associatedTariffId {
      tariff = self.activeSession.user.tariffs[tariffId]
      // Add fix cost to consumption
      let fixCost = tariff!.getConnectionCostForHour(timestamp: Date())
      self.displayedTotalValues.consumption.cost.value = fixCost?.value ?? 0
    }
    
    _ = hostBox.devices.values.map { (singleDevice: Device) ->() in
      self.displayedTotalValues.devicesUsage.attached += 1
      
      if let lastValue = self.activeSession.user.getLastPowerValue(boxId: hostBox.id, deviceId: singleDevice.id) {
        
        if lastValue.power.value > 0 {
          // Device producing
          self.displayedTotalValues.devicesUsage.active += 1
          self.displayedTotalValues.production.power.value += abs(lastValue.power.value)
          self.displayedTotalValues.production.power.unit = lastValue.power.unit
          if tariff != nil {
            if let amount = tariff!.getProductionAmount(powerValue: lastValue.power, timestamp: lastValue.timestamp) {
              self.displayedTotalValues.production.revenue.value += amount.value
            }
          }
        } else if lastValue.power.value < 0 {
          // Device consuming
          self.displayedTotalValues.devicesUsage.active += 1
          self.displayedTotalValues.consumption.power.value += abs(lastValue.power.value)
          self.displayedTotalValues.consumption.power.unit = lastValue.power.unit
          if tariff != nil {
            if let amount = tariff!.getConsumptionAmount(powerValue: lastValue.power, timestamp: lastValue.timestamp) {
              self.displayedTotalValues.consumption.cost.value += amount.value
            }
          }
        }
      }
    }
    
    self.totalsDataSource.onNext(displayedTotalValues)
  }
  
  func updateTableData(devicesNewStatus: [Device]) {
    self.displayedDevices = devicesNewStatus.sorted()
    tableDataSource.onNext(self.displayedDevices)
  }
  
  func selectedDeviceForInfo(_ position: Int){
    print("Showing information of device: \(self.displayedDevices[position].id) - \(self.displayedDevices[position].label ?? "")")
    self.isDeviceInfoSelected.onNext((self.activeSession, boxId: self.selectedBox.id, deviceId: self.displayedDevices[position].id))
  }
}

struct DevicesTotalValues {
  var devicesUsage: (attached: Int, active: Int)
  var production: (power: PowerValue, revenue: Amount)
  var consumption: (power: PowerValue, cost: Amount)
  
  init () {
    let emptyPowerValue = PowerValue(value: 0.0, type: PowerType.active)
    let emptyAmount = Amount(value: 0, currencySymbol: "")
    
    self.devicesUsage = (0,0)
    self.production = (power: emptyPowerValue, revenue: emptyAmount)
    self.consumption = (power: emptyPowerValue, cost: emptyAmount)
  }
}
