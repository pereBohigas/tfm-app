import UIKit
import RxSwift
import RxCocoa

class BoxesViewController: UIViewController, Storyboarded {
  static var storyboard = AppStoryboard.boxes
  private let disposeBag = DisposeBag()
  
  var tariffsButton: UIBarButtonItem = UIBarButtonItem(title: "Tariffs", style: .plain, target: self, action: nil)
  var viewModel: BoxesViewModel?
  
  // UI
  @IBOutlet weak var tableView: UITableView!
  
  @IBOutlet weak var productionActiveValueLabel: UILabel! {
    didSet {
      productionActiveValueLabel.font = productionActiveValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var productionActiveUnitLabel: UILabel!
  @IBOutlet weak var productionApparentValueLabel: UILabel! {
    didSet {
      productionApparentValueLabel.font = productionApparentValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var productionApparentUnitLabel: UILabel!
  @IBOutlet weak var productionRevenueValueLabel: UILabel! {
    didSet {
      productionRevenueValueLabel.font = productionRevenueValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var productionRevenueUnitLabel: UILabel!
  
  @IBOutlet weak var consumptionActiveValueLabel: UILabel! {
     didSet {
       consumptionActiveValueLabel.font = consumptionActiveValueLabel.font.monospacedDigitFont
     }
   }
  @IBOutlet weak var consumptionActiveUnitLabel: UILabel!
  @IBOutlet weak var consumptionApparentValueLabel: UILabel! {
     didSet {
       consumptionApparentValueLabel.font = consumptionApparentValueLabel.font.monospacedDigitFont
     }
   }
  @IBOutlet weak var consumptionApparentUnitLabel: UILabel!
  @IBOutlet weak var consumptionCostValueLabel: UILabel! {
     didSet {
       consumptionCostValueLabel.font = consumptionCostValueLabel.font.monospacedDigitFont
     }
   }
  @IBOutlet weak var consumptionCostUnitLabel: UILabel!
  
  override func viewDidLoad() {
    title = "Boxes"
    self.tableView.contentInset.bottom = 84
    
    navigationItem.leftBarButtonItems = [tariffsButton]
    
    self.setUpBindings()
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    viewModel?.isPolling.accept(true)
    super .viewWillAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    viewModel?.isPolling.accept(false)
    super .viewWillDisappear(animated)
  }
  
  private func setUpBindings() {
    guard let viewModel = self.viewModel, let tableView = self.tableView else { return }
    
    viewModel.tableDataSource
      .bind(to: tableView.rx.items(cellIdentifier: "BoxCell")) { row, box, cell in
        if let boxCell = cell as? BoxCell {
          let user = viewModel.activeSession.user
          
          // Label
          boxCell.boxIdentificationLabel.text = box.label ?? box.id
          
          // Power activity
          let currentPower = user.getLastPowerValue(boxId: box.id)
          (boxCell.powerValueLabel.text, boxCell.powerUnitLabel.text) = user.formatEnergyValue(currentPower?.power)
          
          // Tariff
          let tariff = user.getTariff(boxId: box.id)
          var fixCost: Amount? = nil
          if tariff != nil {
            boxCell.electricityTariffLabel.text = tariff!.description
            fixCost = tariff!.getConnectionCostForHour(timestamp: currentPower?.timestamp)
          } else {
            boxCell.electricityTariffLabel.text = "No tariff associated"
          }
          
          // Usage profile
          if tariff != nil {
            boxCell.usageProfileView.image = UIImage(named: "icon_" + tariff!.usageProfile.rawValue)
          } else {
            boxCell.usageProfileView.image = UIImage()
          }
          
          // Cell apperance
          if currentPower == nil || currentPower!.power.value == 0 {
            // Box off
            if let lastActiveDate: Date = box.lastTimeActive {
              let timeIntervalString = Date().timeIntervalSince(lastActiveDate).stringFromTimeInterval()
              boxCell.activeDevicesOrInactivityTimeLabel.text = "last time with activity \(timeIntervalString) ago"
            } else {
              boxCell.activeDevicesOrInactivityTimeLabel.text = "inactivity time not available"
            }
            if fixCost != nil {
              (boxCell.balanceValueLabel.text, boxCell.balanceUnitLabel.text)  = user.formatAmount(fixCost)
            } else {
              (boxCell.balanceValueLabel.text, boxCell.balanceUnitLabel.text)  = user.formatAmount(nil)
            }
          } else {
            // Active devices
            let activeDevices: Int = box.currentPower.last?.activeDevices ?? 0
            boxCell.activeDevicesOrInactivityTimeLabel.text = "\(activeDevices) of its devices are active"
            if currentPower!.power.value > 0 {
              // Box producing energy
              boxCell.cellBackground.backgroundColor = AppColors.mainLightGreen
              // Calculate revenue
              let variableRevenue = tariff?.getProductionAmount(powerValue: currentPower?.power, timestamp: currentPower?.timestamp)
              let revenue = viewModel.calculateBoxBalance(fix: fixCost, variable: variableRevenue)
              (boxCell.balanceValueLabel.text, boxCell.balanceUnitLabel.text) = user.formatAmount(revenue)
            } else {
              // Box consuming energy
              boxCell.cellBackground.backgroundColor = AppColors.consumingBlue
              let variableCost = tariff?.getConsumptionAmount(powerValue: currentPower?.power, timestamp: currentPower?.timestamp)
              let cost = viewModel.calculateBoxBalance(fix: fixCost, variable: variableCost)
              (boxCell.balanceValueLabel.text, boxCell.balanceUnitLabel.text) = user.formatAmount(cost)
            }
          }
          
          boxCell.infoButton.rx.tap
            .bind {
              viewModel.selectBoxForInfo(row)
            }
            .disposed(by: boxCell.disposeBag)
        }
        cell.selectionStyle = .none
    }
    .disposed(by: self.disposeBag)
    
    viewModel.totalsDataSource
      .observeOn(MainScheduler.instance)
      .subscribe (onNext: { [weak self] newTotals in
        let user = viewModel.activeSession.user
        
        let productionActivePower = user.formatEnergyValue(newTotals.production.active)
        self?.productionActiveValueLabel.text = productionActivePower.value
        self?.productionActiveUnitLabel.text = productionActivePower.unit
        
        let productionApparentPower = user.formatEnergyValue(newTotals.production.apparent)
        self?.productionApparentValueLabel.text = productionApparentPower.value
        self?.productionApparentUnitLabel.text = productionApparentPower.unit
        
        let productionRevenue = user.formatAmount(newTotals.production.revenue)
        self?.productionRevenueValueLabel.text = productionRevenue.value
        self?.productionRevenueUnitLabel.text = productionRevenue.unit
        
        let consumptionActivePower = user.formatEnergyValue(newTotals.consumption.active)
        self?.consumptionActiveValueLabel.text = consumptionActivePower.value
        self?.consumptionActiveUnitLabel.text = consumptionActivePower.unit
        
        let consumptionApparentPower = user.formatEnergyValue(newTotals.consumption.apparent)
        self?.consumptionApparentValueLabel.text = consumptionApparentPower.value
        self?.consumptionApparentUnitLabel.text = consumptionApparentPower.unit
        
        let consumptionCostValueLabel = user.formatAmount(newTotals.consumption.cost)
        self?.consumptionCostValueLabel.text = consumptionCostValueLabel.value
        self?.consumptionCostUnitLabel.text = consumptionCostValueLabel.unit
      })
      .disposed(by: self.disposeBag)
    
    self.tariffsButton.rx.tap
      .bind {
        viewModel.showTariffsScene()
      }
      .disposed(by: disposeBag)
    
    tableView.rx.itemSelected
      .subscribe(onNext: { [weak self] indexPath in
        viewModel.selectBoxForContent(indexPath.row)
      })
      .disposed(by: self.disposeBag)
  }
}
