import UIKit
import RxSwift

class BoxesCoordinator: BaseCoordinator {
  private let disposeBag = DisposeBag()
  private let viewModel: BoxesViewModel
  
  init(session: Session) {
    self.viewModel = BoxesViewModel(session: session)
  }
  
  override func start() {
    let viewController = BoxesViewController.instantiate()
    viewController.viewModel = self.viewModel
    
    UIView.transition(with: self.navigationController.view, duration: 1.0, options: [.transitionCrossDissolve], animations: {
      self.navigationController.viewControllers = [viewController]
    }, completion: nil)
    
    self.setUpBindings()
  }
  
  func presentTariffsScene(session: Session) {
    let tariffsCoordinator = TariffsCoordinator(session: session)
    tariffsCoordinator.navigationController = self.navigationController
    self.coordinate(to: tariffsCoordinator)
  }
  
  func presentDevicesScene(session: Session, boxId: String) {
    // Init fails when a not existing box id is given
    guard let devicesCoordinator = DevicesCoordinator(session: session, boxId: boxId) else { return }
    devicesCoordinator.navigationController = self.navigationController
    self.coordinate(to: devicesCoordinator)
  }
  
  func presentDetailsScene(session: Session, boxId: String) {
    // Init fails when a not existing box id is given
    guard let detailsCoordinator = DetailsCoordinator(session: session, boxId: boxId) else { return }
    detailsCoordinator.navigationController = self.navigationController
    self.coordinate(to: detailsCoordinator)
  }
  
  private func setUpBindings() {
    self.viewModel.isShowingTariffsManager
      .observeOn(MainScheduler.instance)
      .bind { [unowned self] activeSession in
        self.presentTariffsScene(session: activeSession)
      }
      .disposed(by: self.disposeBag)
    
    self.viewModel.isBoxSelectedForContent
      .observeOn(MainScheduler.instance)
      .bind {[unowned self] activeSession, selectedBoxId in
        self.presentDevicesScene(session: activeSession, boxId: selectedBoxId)
      }
      .disposed(by: self.disposeBag)
    
    self.viewModel.isBoxSelectedForInformation
      .observeOn(MainScheduler.instance)
      .bind {[unowned self] activeSession, selectedBoxId in
        self.presentDetailsScene(session: activeSession, boxId: selectedBoxId)
      }
      .disposed(by: self.disposeBag)
  }
}
