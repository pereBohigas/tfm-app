import Foundation
import RxSwift
import RxRelay
import RxCocoa

class TariffsViewModel {
  let disposeBag = DisposeBag()
  var activeSession: Session
  
  // UI
  var displayedTariffs: [ElectricityTariff]
  let tableDataSource = BehaviorSubject(value: [ElectricityTariff]())
  
  // Events
  let isShowBoxesSceneSelected = BehaviorRelay<Bool>(value: false)
  let isTariffManagerSelectedForTariff = PublishSubject<(Session, String)>()
  let isTariffManagerSelectedForNewTariff = PublishSubject<(Session)>()
  
  init(session: Session) {
    self.activeSession = session
    self.displayedTariffs = [ElectricityTariff]()
    
    self.loadTariffs()
    
    self.setUpBindings()
  }
  
  func loadTariffs() {
    self.displayedTariffs = self.activeSession.user.tariffs.values.map { $0 }
    self.tableDataSource.onNext(self.displayedTariffs)
  }
  
  private func setUpBindings() {
  }
  
  func showTariffsManagerForNewTariff() {
    print("Showing tariff manager for new tariff")
    self.isTariffManagerSelectedForNewTariff.onNext((self.activeSession))
  }
  
  func showTariffsManager(_ position: Int) {
    print("Showing tariff manager for tariff: \(self.displayedTariffs[position].id) - \(self.displayedTariffs[position].description)")
    self.isTariffManagerSelectedForTariff.onNext((self.activeSession, self.displayedTariffs[position].id))
  }
}
