import UIKit
import RxSwift
import RxCocoa

class TariffsViewController: UIViewController, Storyboarded {
  static var storyboard = AppStoryboard.tariffs
  private let disposeBag = DisposeBag()
  
  var backButton: UIBarButtonItem?
  var addButton: UIBarButtonItem?
  var viewModel: TariffsViewModel?
  
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Tariffs"
    
    backButton = UIBarButtonItem(title: "Boxes", style: .plain, target: self, action: nil)
    
    addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)
    
    self.navigationItem.leftBarButtonItem = backButton
    self.navigationItem.rightBarButtonItem = addButton
    
    setUpBindings()
  }
  
  private func setUpBindings() {
    guard let viewModel = self.viewModel, let tableView = self.tableView else { return }
    
    viewModel.tableDataSource
      .bind(to: tableView.rx.items(cellIdentifier: "TariffCell")) { row, tariff, cell in
        if let tariffCell = cell as? TariffCell {
          // Label
          tariffCell.tariffIdentificationLabel.text = !tariff.denomination.isBlank() ? tariff.denomination : tariff.id
          
          // Usage profile
          tariffCell.iconUsageProfileView.image = UIImage(named: "icon_" + tariff.usageProfile.rawValue)
          
          // Provider
          tariffCell.powerProviderLabel.text = tariff.provider
          
          // Connection price
          tariffCell.connectionPriceValueLabel.text = "\(tariff.fixedRate ?? 0)"
          tariffCell.connectionPriceUnitLabel.text = " " + tariff.currencySymbol + " / month"
          
          // Consumption price
          tariffCell.consumptionPriceValueLabel.text = "\(tariff.consumptionPrice ?? 0)"
          tariffCell.consumptionPriceUnitLabel.text = " " + tariff.currencySymbol + " / " + tariff.powerUnit + "h"
          
          // Production revenue
          tariffCell.productionRevenueValueLabel.text = "\(tariff.productionRevenue ?? 0)"
          tariffCell.productionRevenueUnitLabel.text = " " + tariff.currencySymbol + " / " + tariff.powerUnit + "h"
          
          tariffCell.modifyButton.rx.tap
            .bind {
              viewModel.showTariffsManager(row)
            }
            .disposed(by: self.disposeBag)
        }
        cell.selectionStyle = .none
    }
      .disposed(by: self.disposeBag)
    
    self.backButton?.rx.tap
      .bind {
        viewModel.isShowBoxesSceneSelected.accept(true)
      }
      .disposed(by: disposeBag)
    
    self.addButton?.rx.tap
      .bind {
        viewModel.showTariffsManagerForNewTariff()
      }
      .disposed(by: disposeBag)
  }
}
