import Foundation
import UIKit
import RxSwift

class TariffsCoordinator: BaseCoordinator {
  private let disposeBag = DisposeBag()
  private var viewModel: TariffsViewModel?
  
  init(session: Session) {
    self.viewModel = TariffsViewModel(session: session)
  }
  
  override func start() {
    let viewController = TariffsViewController.instantiate()
    viewController.viewModel = self.viewModel
    
    self.presentWithFlipAnimation(viewController: viewController)
    setUpBindings()
  }
  
  private func presentWithFlipAnimation(viewController: UIViewController) {
    UIView.transition(with: self.navigationController.view, duration: 1.0, options: [.transitionFlipFromLeft], animations: {
      self.navigationController.pushViewController(viewController, animated: false)
    }, completion: nil)
  }
  
  private func dismissWithFlipAnimation() {
    UIView.transition(with: self.navigationController.view, duration: 1.0, options: .transitionFlipFromRight, animations: {
      self.navigationController.popViewController(animated: false)
      self.parentCoordinator?.release(coordinator: self)
    }, completion: nil)
  }
  
  func reloadData(session activeSession: Session) {
    self.viewModel?.activeSession = activeSession
    self.viewModel?.loadTariffs()
  }
  
  func presentTariffManagerScene(session: Session) {
    let tariffManagerCoordinator = TariffManagerCoordinator(session: session)
    tariffManagerCoordinator.navigationController = self.navigationController
    self.coordinate(to: tariffManagerCoordinator)
  }
  
  func presentTariffManagerScene(session: Session, tariffId: String) {
    // Init fails when a not existing tariff id is given
    guard let tariffManagerCoordinator = TariffManagerCoordinator(session: session, tariffId: tariffId) else { return }
    tariffManagerCoordinator.navigationController = self.navigationController
    self.coordinate(to: tariffManagerCoordinator)
  }
  
  private func setUpBindings() {
    self.viewModel?.isShowBoxesSceneSelected
      .observeOn(MainScheduler.instance)
      .filter { $0 == true }
      .bind { [unowned self] _ in
        self.dismissWithFlipAnimation()
      }
      .disposed(by: self.disposeBag)
    
    self.viewModel?.isTariffManagerSelectedForNewTariff
      .observeOn(MainScheduler.instance)
      .bind { [unowned self] activeSession in
        self.presentTariffManagerScene(session: activeSession)
      }
      .disposed(by: self.disposeBag)
    
    self.viewModel?.isTariffManagerSelectedForTariff
      .observeOn(MainScheduler.instance)
      .bind { [unowned self] activeSession, selectedTariff in
        self.presentTariffManagerScene(session: activeSession, tariffId: selectedTariff)
      }
      .disposed(by: self.disposeBag)
  }
}
