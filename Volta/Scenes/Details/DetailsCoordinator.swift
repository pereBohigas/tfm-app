import RxSwift

class DetailsCoordinator: BaseCoordinator {
  private let disposeBag = DisposeBag()
  private let viewModel: DetailsViewModel
  
  init?(session activeSession: Session, boxId: String) {
    guard let detailsViewModel = DetailsViewModel(session: activeSession, boxId: boxId) else { return nil }
    self.viewModel = detailsViewModel
  }
  
  init?(session activeSession: Session, boxId: String, deviceId: String) {
    guard let detailsViewModel = DetailsViewModel(session: activeSession, boxId: boxId, deviceId: deviceId) else { return nil }
    self.viewModel = detailsViewModel
  }
  
  override func start() {
    let viewController = DetailsViewController.instantiate()
    viewController.viewModel = self.viewModel
    
    viewController.modalPresentationStyle = .pageSheet
    self.navigationController.present(viewController, animated: true)
    self.setUpBindings()
  }
  
  func dismiss() {
    self.parentCoordinator?.release(coordinator: self)
  }
  
  private func setUpBindings() {
    self.viewModel.didDismiss
      .observeOn(MainScheduler.instance)
      .filter { $0 == true }
      .bind { [unowned self] _ in
        self.dismiss()
      }
      .disposed(by: disposeBag)
  }
}
