import Foundation
import RxSwift
import RxCocoa

class DetailsViewModel {
  let disposeBag = DisposeBag()
  
  var activeSession: Session
  let elementMirror: Mirror
  
  // UI
  let displayedVoltaElement: (id: String, label: String?, element: AnyObject)
  let tableDataSource = BehaviorSubject(value: [(name: String, value: Any)]())
  
  // Events
  let isPolling = BehaviorRelay<Bool>(value: false)
  let didDismiss = PublishSubject<Bool>()
  
  // Init for Box
  init?(session: Session, boxId: String) {
    self.activeSession = session
    guard let box = activeSession.user.boxes[boxId] else { return nil }
    self.displayedVoltaElement = (id: box.id, label: box.label, element: box as AnyObject)
    self.elementMirror = Mirror(reflecting: box )
    
    extractElementProperties()
  }
  
  // Init for Device
  init?(session: Session, boxId: String, deviceId: String) {
    self.activeSession = session
    guard let device = activeSession.user.boxes[boxId]?.devices[deviceId] else { return nil }
    self.displayedVoltaElement = (id: device.id, label: device.label, element: device as AnyObject)
    self.elementMirror = Mirror(reflecting: device)
    
    extractElementProperties()
  }
  
  func extractElementProperties() {
    var properties = [(name: String, value: Any)]()
    for (name, value) in elementMirror.children {
      guard let stringName = name else { return }
      if stringName != "currentPower" && stringName != "accumulatedPower" && stringName != "maximumValues" && stringName != "devices" {
        properties.append((name: stringName, value: value))
      } else if stringName == "devices" {
        if let devices = value as? [String: Device] {
          let sortedDevices = devices.sorted(by: { Int($0.0) != nil && Int($1.0) != nil ? Int($0.0)! < Int($1.0)! : $0.0 < $1.0})
          var devicesArray = [(id: String, label: String?)]()
          for (_, value) in sortedDevices {
            devicesArray.append((id: value.id, label: value.label))
          }
          properties.append((name: stringName, value: devicesArray))
        } else {
          properties.append((name: stringName, value: value))
        }
      }
    }
    tableDataSource.onNext(properties)
  }
}
