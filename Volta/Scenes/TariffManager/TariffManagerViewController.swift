import UIKit
import RxSwift
import RxCocoa

class TariffManagerViewController: UIViewController, Storyboarded {

  static var storyboard = AppStoryboard.tariffManager
  private let disposeBag = DisposeBag()
  
  var viewModel: TariffManagerViewModel?
  
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var doneButton: UIButton! {
     didSet {
       doneButton.isEnabled = false
       doneButton.alpha = 0.3
     }
   }
  @IBOutlet weak var removeButton: UIButton!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  
  var selectedRow: Int = 0
  
  override func viewDidLoad() {
    
    
    //self.view.backgroundColor = AppColors.mainLightBlack.lighter(by: 69)
    self.setUpBindings()
    super.viewDidLoad()
  }
  
  private func setUpBindings() {
    guard let viewModel = self.viewModel, let tableView = self.tableView else { return }
    
    viewModel.tableDataSource
      .bind(to: tableView.rx.items) { table, index, property in
        var value: String?
        
        switch property.label {
        case "id":
          value = property.value as? String
          return self.makeSimplePropertyCell(property: property.label, value: value, from: table)
        case "usageProfile":
          let allUsages: [String] = UsageProfile.allCases.map { $0.rawValue }
          value = (property.value as? UsageProfile).map { $0.rawValue }
          return self.makeSelectablePropertyCell(property: property.label, values: allUsages, selection: value, from: table)
        case "fixedRate":
          if let decimalValue = property.value as? Decimal {
            value = "\(decimalValue)"
          }
          let unit = " " + viewModel.displayedPowerTariff.currencySymbol + " / month"
          return self.makePartialEditablePropertyCell(property: property.label, editableValue: value ?? "0", uneditableValue: unit, from: table)
        case "consumptionPrice", "productionRevenue":
          if let decimalValue = property.value as? Decimal {
            value = "\(decimalValue)"
          }
          let unit = " " + viewModel.displayedPowerTariff.currencySymbol + " / " + viewModel.displayedPowerTariff.powerUnit + "h"
          return self.makePartialEditablePropertyCell(property: property.label, editableValue: value ?? "0", uneditableValue: unit, from: table)
        default:
          // Used in cases of label, provider, currencyCode
          value = property.value as? String
      }
      return self.makeEditablePropertyCell(property: property.label, value: value ?? (property.value as? String), from: table)
    }
    .disposed(by: self.disposeBag)
    
    cancelButton.rx.tap
      .bind {
        print("Dismissing Tariff manager view without saving changes")
        self.dismiss(animated: true, completion: {
          viewModel.didDismissSavingChanges.onNext(false)
        })
      }
      .disposed(by: self.disposeBag)
    
    removeButton.rx.tap
      .bind {
        viewModel.didRemoveTariff.onNext(())
      }
      .disposed(by: self.disposeBag)
    
    doneButton.rx.tap
      .bind {
        print("Dismissing Tariff manager view saving changes")
        self.dismiss(animated: true, completion: {
          viewModel.didDismissSavingChanges.onNext(true)
        })
      }
      .disposed(by: self.disposeBag)
    
    viewModel.isEditing
      .bind { [unowned self] isEditing in
        if isEditing {
          self.titleLabel.text = "Editing tariff"
        } else {
          self.titleLabel.text = "Creating new tariff"
        }
      }
      .disposed(by: self.disposeBag)
    
    viewModel.wrongValueForProperty
      .bind { [unowned self] errorDescription in
        self.showAlert(title: "Wrong value", message: errorDescription)
      }
      .disposed(by: self.disposeBag)
    
    viewModel.areChangesToSave
      .bind { [unowned self] areChanges in
        if areChanges {
          self.doneButton.isEnabled = true
          self.doneButton.alpha = 1.0
        } else {
          self.doneButton.isEnabled = false
          self.doneButton.alpha = 0.3
        }
      }
      .disposed(by: self.disposeBag)
  }
  
  private func makeSimplePropertyCell(property: String, value: String?, from table: UITableView) -> SimplePropertyCell {
    let cell = table.dequeueReusableCell(withIdentifier: "SimplePropertyCell")! as! SimplePropertyCell
    
    cell.propertyLabel.text = property.camelCaseToWords()
    cell.valueLabel.font = cell.valueLabel.font.withSize(12)
    if let stringValue = value {
      cell.valueLabel.text = stringValue
    } else {
      cell.valueLabel.text = "information not available"
      cell.propertyLabel.isEnabled = false
      cell.valueLabel.isEnabled = false
    }
    return cell
  }
  
  private func makeEditablePropertyCell(property: String, value: String?, from table: UITableView) -> EditablePropertyCell {
    let cell = table.dequeueReusableCell(withIdentifier: "EditablePropertyCell")! as! EditablePropertyCell
    
    cell.propertyLabel.text = property.camelCaseToWords()
    if let stringValue = value {
      cell.propertyValueTextField.text = stringValue
    } else {
      cell.propertyValueTextField.placeholder = "missing information"
    }
    
    cell.propertyValueTextField.rx.controlEvent([.editingDidEnd, .editingDidEndOnExit])
      .asObservable()
      .subscribe(onNext: { [unowned self] _ in
        print("Editable property \(property) changed, current value: \(cell.propertyValueTextField.text ?? "")")
        self.viewModel?
          .hasPropertyChanged
          .onNext((label: property, value: cell.propertyValueTextField.text ?? ""))
      })
      .disposed(by: disposeBag)
    return cell
  }
  
  private func makePartialEditablePropertyCell(property: String, editableValue: String, uneditableValue: String, from table: UITableView) -> PartiallyEditablePropertyCell {
    let cell = table.dequeueReusableCell(withIdentifier: "PartiallyEditablePropertyCell") as! PartiallyEditablePropertyCell
    
    cell.propertyLabel.text = property.camelCaseToWords()
    cell.editableValueTextField.text = editableValue
    cell.uneditableValueLabel.text = uneditableValue
    
    cell.editableValueTextField.rx.controlEvent([.editingChanged, .editingDidEnd, .editingDidEndOnExit])
      .asObservable()
      .subscribe(onNext: { [unowned self] _ in
        print("Editable property \(property) changed, current value: \(cell.editableValueTextField.text ?? "")")
        self.viewModel?
          .hasPropertyChanged
          .onNext((label: property, value: cell.editableValueTextField.text ?? ""))
      })
      .disposed(by: disposeBag)
    return cell
  }
  
  private func makeSelectablePropertyCell(property: String, values: [String], selection: String?, from table: UITableView) -> SelectablePropertyCell {
    let cell = table.dequeueReusableCell(withIdentifier: "SelectablePropertyCell")! as! SelectablePropertyCell
    cell.propertyLabel.text = property.camelCaseToWords()
    cell.propertySelector.removeAllSegments()
    values.enumerated().forEach { (index, value) in cell.propertySelector.insertSegment(withTitle: value.camelCaseToWords(), at: index, animated: false) }
    let selectedIndex = values.firstIndex(of: selection ?? "")
    cell.propertySelector.selectedSegmentIndex = selectedIndex ?? 0
    cell.propertySelector.selectedSegmentTintColor = UIColor.lightGray
    
    cell.propertySelector.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
    cell.propertySelector.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.normal)
    
    cell.propertySelector.rx.selectedSegmentIndex
      .asObservable()
      .skip(1)
      .distinctUntilChanged()
      .subscribe(onNext: { index in
        print("Selectable property \(property) changed, current value: \(cell.propertySelector.titleForSegment(at: index) ?? "")")
        self.viewModel?
          .hasPropertyChanged
          .onNext((label: property, value: cell.propertySelector.titleForSegment(at: index) ?? ""))
      })
      .disposed(by: self.disposeBag)
    return cell
  }
}
