import Foundation
import RxSwift
import RxCocoa

class TariffManagerViewModel {
  let disposeBag = DisposeBag()
  
  var activeSession: Session
  lazy var tariffMirror: Mirror = Mirror(reflecting: displayedPowerTariff)
  
  // UI
  var displayedPowerTariff: ElectricityTariff
  let tableDataSource = BehaviorSubject(value: [(label: String, value: Any)]())
  
  // Events
  let isEditing = BehaviorRelay<Bool>(value: false)
  let isPolling = BehaviorRelay<Bool>(value: false)
  let didDismiss = PublishSubject<Session>()
  let didDismissSavingChanges = PublishSubject<Bool>()
  let didRemoveTariff = PublishSubject<Void>()
  let wrongValueForProperty = PublishSubject<String>()
  let hasPropertyChanged = PublishSubject<(label: String, value: Any)>()
  let areChangesToSave = PublishSubject<Bool>()
  
  // Init for new Tariff
  init(session: Session) {
    self.activeSession = session
    self.displayedPowerTariff = ElectricityTariff()
    self.isEditing.accept(false)
    
    self.setUpBindings()
    self.reloadTariffProperties()
  }
  
  // Init for existing Tariff
  init?(session: Session, tariffId: String) {
    self.activeSession = session
    guard let tariff = activeSession.user.tariffs[tariffId] else { return nil }
    self.displayedPowerTariff = tariff
    self.isEditing.accept(true)
    
    self.setUpBindings()
    self.reloadTariffProperties()
  }
  
  func reloadTariffProperties() {
    self.tariffMirror = Mirror(reflecting: displayedPowerTariff)
    var properties = [(label: String, value: Any)]()
    for (label, value) in tariffMirror.children {
      guard let stringLabel = label else { return }
      if stringLabel != "currencySymbol" {
        properties.append((label: stringLabel, value: value))
      }
    }
    tableDataSource.onNext(properties)
  }
  
  private func setUpBindings() {
    self.hasPropertyChanged
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { [unowned self] (label, value) in
        switch label{
        case "label":
          if let newDenomination = value as? String, !newDenomination.isBlank() {
            self.displayedPowerTariff.denomination = newDenomination
            self.areChangesToSave.onNext(true)
          }
        case "provider":
          if let newProvider = value as? String, !newProvider.isBlank() {
            self.displayedPowerTariff.provider = newProvider
            self.areChangesToSave.onNext(true)
          }
        case "currencyCode":
          if let newCurrencyCode = value as? String, !newCurrencyCode.isBlank() {
            if Locale.isoCurrencyCodes.contains(newCurrencyCode) {
              self.displayedPowerTariff.currencyCode = newCurrencyCode
              self.areChangesToSave.onNext(true)
            } else {
              self.wrongValueForProperty.onNext("Invalid currency code")
            }
            self.reloadTariffProperties()
          }
        case "usageProfile":
          if let newUsageProfile = UsageProfile(rawValue: (value as! String).lowercased()) {
            self.displayedPowerTariff.usageProfile = newUsageProfile
            self.areChangesToSave.onNext(true)
          }
          self.reloadTariffProperties()
        case "connectionPrice":
          if let newValue = Double(value as! String) {
            self.displayedPowerTariff.fixedRate = Decimal(newValue)
            self.areChangesToSave.onNext(true)
          } else {
            self.wrongValueForProperty.onNext("Invalid value")
            self.reloadTariffProperties()
          }
        case "consumptionPrice":
          if let newValue = Double(value as! String) {
            self.displayedPowerTariff.consumptionPrice = Decimal(newValue)
            self.areChangesToSave.onNext(true)
          } else {
            self.wrongValueForProperty.onNext("Invalid value")
            self.reloadTariffProperties()
          }
        case "productionRevenue":
          if let newValue = Double(value as! String) {
            self.displayedPowerTariff.productionRevenue = Decimal(newValue)
            self.areChangesToSave.onNext(true)
          } else {
            self.wrongValueForProperty.onNext("Invalid value")
            self.reloadTariffProperties()
          }
        default:
          break
        }
      })
      .disposed(by: self.disposeBag)
    
    self.didDismissSavingChanges
      .observeOn(MainScheduler.instance)
      .bind { [unowned self] savingChanges in
        if savingChanges {
          self.activeSession.user.tariffs[self.displayedPowerTariff.id] = self.displayedPowerTariff
        }
        self.didDismiss.onNext(self.activeSession)
      }
      .disposed(by: self.disposeBag)
    
    self.didRemoveTariff
      .bind { [unowned self] _ in
        self.activeSession.user.tariffs.removeValue(forKey: self.displayedPowerTariff.id)
        self.displayedPowerTariff = ElectricityTariff()
        self.reloadTariffProperties()
        self.isEditing.accept(false)
        self.areChangesToSave.onNext(false)
      }
      .disposed(by: self.disposeBag)
  }
  
  private func saveModifiedTariff() {
    
  }
}
