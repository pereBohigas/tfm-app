import UIKit
import RxSwift

protocol SignInListener {
  func didSignIn(session: Session)
}

class AppCoordinator: BaseCoordinator {
  lazy var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
  var activeSession: Session? = nil
  
  override init() {
  }
  
  override func start() {
    self.window?.makeKeyAndVisible()
    self.window?.rootViewController = self.navigationController
    
    setNavigationBar()
    presentSignInScene()
  }
  
  func presentSignInScene() {
    self.removeChildCoordinators()
    
    self.navigationController.navigationBar.isHidden = true
    
    #if MOCKSERVER
    let client = MockServerApiClient()
    #else
    let client = VoltaApiClient()
    #endif
    
    let coordinator = SignInCoordinator(client: client)
    coordinator.navigationController = self.navigationController
    self.coordinate(to: coordinator)
  }
  
  func presentBoxesScene() {
    self.removeChildCoordinators()
    
    self.navigationController.navigationBar.isHidden = false
    
    let boxesCoordinator = BoxesCoordinator(session: self.activeSession!)
    boxesCoordinator.navigationController = self.navigationController
    self.coordinate(to: boxesCoordinator)
  }
  
  func setNavigationBar() {
    //        TODO: Evaluate it
    //        self.navigationController.navigationBar.barTintColor = AppColors.mainDarkBlack.withAlphaComponent(1.0)
    self.navigationController.navigationBar.tintColor = .white
    self.navigationController.navigationBar.barStyle = .black
  }
}

extension AppCoordinator: SignInListener {
  func didSignIn(session: Session) {
    self.activeSession = session
    presentBoxesScene()
    self.navigationController.showNotification(message: "Signed in")
  }
}
