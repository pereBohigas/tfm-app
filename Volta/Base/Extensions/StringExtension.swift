import Foundation

extension String {
  func camelCaseToWords() -> String {
    return unicodeScalars.reduce("") {
      if CharacterSet.uppercaseLetters.contains($1) {
        if $0.count > 0 {
          return ($0 + " " + String($1).lowercased())
        }
      }
      return $0 + String($1)
    }.capitalizingFirstLetter()
  }
  
  func capitalizingFirstLetter() -> String {
    return prefix(1).capitalized + dropFirst()
  }
  
  func isBlank() -> Bool {
    for character in self {
      if !character.isWhitespace {
          return false
      }
    }
    return true
  }
}
