import Foundation

extension TimeInterval{
  
  func stringFromTimeInterval() -> String {
    
    let time = NSInteger(self)
    
    let seconds = time % 60
    let minutes = (time / 60) % 60
    let hours = (time / 3600)
    let days = (time / 86400)
    
    var timeString: String = String()
    
    if days != 0 {
      timeString = String(format: "%2d ",days)
      if days == 1 {
        // use singular form of day
        timeString.append("day")
      } else if days > 1 {
        // use plural form of day
        timeString.append("days")
      }
    } else if hours != 0 {
      timeString = String(format: "%2d ",hours)
      if hours == 1 {
        // use singular form of hour
        timeString.append("hour")
      } else if hours > 1 {
        // use plural form of hour
        timeString.append("hours")
      }
    } else if minutes != 0 {
      timeString = String(format: "%2d ",minutes)
      if minutes == 1 {
        // use singular form of minute
        timeString.append("minute")
      } else {
        // use plural form of minute
        timeString.append("minutes")
      }
    } else {
      timeString = String(format: "%2d ",seconds)
      if seconds == 1 {
        // use singular form of second
        timeString.append("second")
      } else {
        // use plural form of second
        timeString.append("seconds")
      }
    }
    
    return timeString
    
  }
}
