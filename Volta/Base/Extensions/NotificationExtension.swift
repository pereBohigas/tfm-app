import Foundation
import UIKit

extension UIViewController {
  
  func showNotification(message: String) {
    let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    self.present(alertController, animated: true) {
      DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
        alertController.dismiss(animated: true, completion: nil)
      }
    }
  }
}
