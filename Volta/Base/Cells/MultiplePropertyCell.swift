import RxSwift
import UIKit

class MultiplePropertyCell: UITableViewCell {
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var propertyLabel: UILabel!
  @IBOutlet weak var subPropertiesTableView: UITableView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.disposeBag = DisposeBag()
  }
}

