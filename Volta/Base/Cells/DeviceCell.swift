import RxSwift
import UIKit

class DeviceCell: UITableViewCell {
  
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var cellBackground: UIView! {
    didSet {
      cellBackground.layer.masksToBounds = false
      cellBackground.layer.cornerRadius = 3.0
      cellBackground.layer.shadowOffset = CGSize(width: -1, height: 1)
      cellBackground.layer.shadowOpacity = 0.2
    }
  }
  @IBOutlet weak var deviceIdentificationLabel: UILabel!
  @IBOutlet weak var powerActivityOrInactivityTimeLabel: UILabel! {
    didSet {
      powerActivityOrInactivityTimeLabel.font = powerActivityOrInactivityTimeLabel.font.monospacedDigitFont
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.powerActivityOrInactivityTimeLabel.font = powerActivityOrInactivityTimeLabel.font.withSize(18)
    self.cellBackground.backgroundColor = .lightGray
    self.disposeBag = DisposeBag()
  }
}
