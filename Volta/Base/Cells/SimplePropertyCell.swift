import RxSwift
import UIKit

class SimplePropertyCell: UITableViewCell {
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var propertyLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel! {
    didSet {
      valueLabel.font = valueLabel.font.monospacedDigitFont
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.valueLabel.font = self.valueLabel.font.withSize(15)
    self.propertyLabel.isEnabled = true
    self.valueLabel.isEnabled = true
    self.disposeBag = DisposeBag()
  }
}
