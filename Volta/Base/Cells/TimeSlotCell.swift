import RxSwift
import UIKit

class TimeSlotCell: UITableViewCell {
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var startTimeLabel: UILabel! {
    didSet {
      startTimeLabel.font = startTimeLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var endTimeLabel: UILabel! {
    didSet {
      endTimeLabel.font = endTimeLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var priceValueLabel: UILabel! {
    didSet {
      priceValueLabel.font = priceValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var priceUnitLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.disposeBag = DisposeBag()
  }
}
