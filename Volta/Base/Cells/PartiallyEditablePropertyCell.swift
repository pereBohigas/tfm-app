import RxSwift
import UIKit

class PartiallyEditablePropertyCell: UITableViewCell {
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var propertyLabel: UILabel!
  @IBOutlet weak var editableValueTextField: UITextField! {
    didSet {
      editableValueTextField.font = editableValueTextField.font?.monospacedDigitFont
    }
  }
  @IBOutlet weak var uneditableValueLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    self.editableValueTextField.font = self.editableValueTextField.font?.withSize(15)
    self.propertyLabel.isEnabled = true
    self.editableValueTextField.isEnabled = true
    self.disposeBag = DisposeBag()
  }
}
