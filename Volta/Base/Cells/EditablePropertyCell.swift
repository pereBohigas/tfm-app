import RxSwift
import UIKit

class EditablePropertyCell: UITableViewCell {
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var propertyLabel: UILabel!
  @IBOutlet weak var propertyValueTextField: UITextField! {
    didSet {
      propertyValueTextField.font = propertyValueTextField.font?.monospacedDigitFont
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    self.propertyValueTextField.font = self.propertyValueTextField.font?.withSize(15)
    self.propertyLabel.isEnabled = true
    self.propertyValueTextField.isEnabled = true
    self.disposeBag = DisposeBag()
  }
}
