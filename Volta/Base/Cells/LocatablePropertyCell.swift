import RxSwift
import UIKit
import MapKit

class LocatablePropertyCell: UITableViewCell {
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var propertyLabel: UILabel!
  @IBOutlet weak var mapView: MKMapView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.disposeBag = DisposeBag()
  }
}
