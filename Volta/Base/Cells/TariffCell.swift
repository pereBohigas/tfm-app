import RxSwift
import UIKit

class TariffCell: UITableViewCell {
  
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var cellBackgroundView: UIView! {
    didSet {
      cellBackgroundView.layer.masksToBounds = false
      cellBackgroundView.layer.cornerRadius = 3.0
      cellBackgroundView.layer.shadowOpacity = 0.2
      cellBackgroundView.layer.shadowRadius = 3.0
      cellBackgroundView.layer.shadowOffset = CGSize(width: -1, height: 1)
      cellBackgroundView.layer.shadowOpacity = 0.2
    }
  }
  
  @IBOutlet weak var iconUsageProfileView: UIImageView!{
    didSet {
      iconUsageProfileView.isUserInteractionEnabled = true
    }
  }
  @IBOutlet weak var tariffIdentificationLabel: UILabel!
  @IBOutlet weak var modifyButton: UIButton!
  
  @IBOutlet weak var powerProviderLabel: UILabel!
  
  @IBOutlet weak var connectionPriceValueLabel: UILabel! {
    didSet {
      connectionPriceValueLabel.font = connectionPriceValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var connectionPriceUnitLabel: UILabel!

  @IBOutlet weak var consumptionPriceValueLabel: UILabel! {
     didSet {
       consumptionPriceValueLabel.font = consumptionPriceValueLabel.font.monospacedDigitFont
     }
   }
  @IBOutlet weak var consumptionPriceUnitLabel: UILabel!
  
  @IBOutlet weak var productionRevenueValueLabel: UILabel! {
     didSet {
       productionRevenueValueLabel.font = productionRevenueValueLabel.font.monospacedDigitFont
     }
   }
  @IBOutlet weak var productionRevenueUnitLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.disposeBag = DisposeBag()
  }
}
