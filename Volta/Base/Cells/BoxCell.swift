import RxSwift
import UIKit

class BoxCell: UITableViewCell {
  
  var disposeBag = DisposeBag()
  
  @IBOutlet weak var cellBackground: UIView! {
    didSet {
      cellBackground.layer.masksToBounds = false
      cellBackground.layer.cornerRadius = 3.0
      cellBackground.layer.shadowOffset = CGSize(width: -1, height: 1)
      cellBackground.layer.shadowOpacity = 0.2
    }
  }
  
  @IBOutlet weak var boxIdentificationLabel: UILabel!
  @IBOutlet weak var usageProfileView: UIImageView!
  @IBOutlet weak var infoButton: UIButton!
  
  @IBOutlet weak var activeDevicesOrInactivityTimeLabel: UILabel! {
    didSet {
      activeDevicesOrInactivityTimeLabel.font = activeDevicesOrInactivityTimeLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var powerValueLabel: UILabel! {
    didSet {
      powerValueLabel.font = powerValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var powerUnitLabel: UILabel!
  
  @IBOutlet weak var electricityTariffLabel: UILabel!
  @IBOutlet weak var balanceValueLabel: UILabel! {
    didSet {
      balanceValueLabel.font = balanceValueLabel.font.monospacedDigitFont
    }
  }
  @IBOutlet weak var balanceUnitLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.cellBackground.backgroundColor = .lightGray
    self.disposeBag = DisposeBag()
  }
}
